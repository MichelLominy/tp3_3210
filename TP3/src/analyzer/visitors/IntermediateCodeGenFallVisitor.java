//package analyzer.visitors;
//
//import analyzer.ast.*;
//
//import java.io.PrintWriter;
//import java.util.HashMap;
//import java.util.Vector;
//
//
///**
// * Ce visiteur explore l'AST et génère du code intermédiaire.
// *
// * @author Félix Brunet
// * @author Doriane Olewicki
// * @author Quentin Guidée
// * @version 2023.02.17
// */
//public class IntermediateCodeGenFallVisitor implements ParserVisitor {
//    public static final String FALL = "fall";
//
//    private final PrintWriter m_writer;
//
//    public HashMap<String, IntermediateCodeGenVisitor.VarType> SymbolTable = new HashMap<>();
//
//    private int id = 0;
//    private int label = 0;
//
//    public IntermediateCodeGenFallVisitor(PrintWriter writer) {
//        m_writer = writer;
//    }
//
//    private String newID() {
//        return "_t" + id++;
//    }
//
//    private String newLabel() {
//        return "_L" + label++;
//    }
//
//    @Override
//    public Object visit(SimpleNode node, Object data) {
//        return data;
//    }
//
//    @Override
//    public Object visit(ASTProgram node, Object data) {
//        String newLabel = newLabel();
//        node.childrenAccept(this, newLabel);
//        // TODO
//        m_writer.println(newLabel);
//        return null;
//    }
//
//    @Override
//    public Object visit(ASTDeclaration node, Object data) {
//        ASTIdentifier id = (ASTIdentifier) node.jjtGetChild(0);
//        SymbolTable.put(id.getValue(), node.getValue().equals("bool") ? IntermediateCodeGenVisitor.VarType.Bool : IntermediateCodeGenVisitor.VarType.Number);
//        return null;
//    }
//
//    @Override
//    public Object visit(ASTBlock node, Object data) {
//        node.childrenAccept(this, data);
//        // TODO
//        return null;
//    }
//
//    @Override
//    public Object visit(ASTScriptProgram node, Object data) {
//        node.childrenAccept(this, data);
//        // TODO
//        return null;
//    }
//
//    @Override
//    public Object visit(ASTScript node, Object data) {
//        node.childrenAccept(this, data);
//        return null;
//    }
//
//    @Override
//    public Object visit(ASTScriptCall node, Object data) {
//        node.childrenAccept(this, data);
//        // TODO
//        return null;
//    }
//
//    @Override
//    public Object visit(ASTStmt node, Object data) {
//        node.childrenAccept(this, data);
//        return null;
//    }
//
//    @Override
//    public Object visit(ASTIfStmt node, Object data) {
//        node.childrenAccept(this, data);
//        // TODO
//        return null;
//    }
//
//    @Override
//    public Object visit(ASTWhileStmt node, Object data) {
//        node.childrenAccept(this, data);
//        // TODO
//        return null;
//    }
//
//    @Override
//    public Object visit(ASTForStmt node, Object data) {
//        node.childrenAccept(this, data);
//        // TODO
//        return null;
//    }
//
//    @Override
//    public Object visit(ASTAssignStmt node, Object data) {
//        String identifier = ((ASTIdentifier) node.jjtGetChild(0)).getValue();
//        //node.jjtGetChild(1).jjtAccept(this, data);
//        // TODO
//        if(SymbolTable.get(identifier).equals(VarType.Bool)) {
//            String ltrue = newLabel();
//            String lfalse = newLabel();
//            String rightSide = (String) node.jjtGetChild(1).jjtAccept(this, new IntermediateCodeGenFallVisitor.BoolLabel(ltrue, lfalse));
//            m_writer.println(ltrue);
//            m_writer.println(identifier + " = 1");
//            m_writer.println("goto " + data);
//            m_writer.println(lfalse);
//            m_writer.println(identifier + " = 0");
//        }else{
//            //Node rightSide = node.jjtGetParent();
//            Object rightSide = node.jjtGetChild(1).jjtAccept(this, data);
//            //String rightSide = (String) node.jjtGetChild(1).jjtAccept(this, data);
//            m_writer.println(identifier + " = " + rightSide);
//
//        }
//        return null;
//    }
//
//    @Override
//    public Object visit(ASTExpr node, Object data) {
//        return node.jjtGetChild(0).jjtAccept(this, data);
//    }
//
//    public Object codeExtAddMul(SimpleNode node, Object data, Vector<String> ops) {
//        // À noter qu'il n'est pas nécessaire de boucler sur tous les enfants.
//        // La grammaire n'accepte plus que 2 enfants maximum pour certaines opérations, au lieu de plusieurs
//        // dans les TPs précédents. Vous pouvez vérifier au cas par cas dans le fichier Langage.jjt.
//        node.childrenAccept(this, data);
//        // TODO
//        return null;
//    }
//
//    @Override
//    public Object visit(ASTAddExpr node, Object data) {
//        return codeExtAddMul(node, data, node.getOps());
//    }
//
//    @Override
//    public Object visit(ASTMulExpr node, Object data) {
//        return codeExtAddMul(node, data, node.getOps());
//    }
//
//    @Override
//    public Object visit(ASTUnaExpr node, Object data) {
//        node.jjtGetChild(0).jjtAccept(this, data);
//        // TODO
//        return null;
//    }
//
//    @Override
//    public Object visit(ASTBoolExpr node, Object data) {
//        node.childrenAccept(this, data);
//        // TODO
//        return null;
//    }
//
//    @Override
//    public Object visit(ASTCompExpr node, Object data) {
//        //node.childrenAccept(this, data);
//        // TODO
//        return node.jjtGetChild(0).jjtAccept(this, data);
//    }
//
//    @Override
//    public Object visit(ASTNotExpr node, Object data) {
//        node.jjtGetChild(0).jjtAccept(this, data);
//        // TODO
//        return null;
//    }
//
//    @Override
//    public Object visit(ASTGenValue node, Object data) {
//        node.jjtGetChild(0).jjtAccept(this, data);
//        // TODO
//        return null;
//    }
//
//    @Override
//    public Object visit(ASTBoolValue node, Object data) {
//        // TODO
//        return null;
//    }
//
//    @Override
//    public Object visit(ASTIdentifier node, Object data) {
//        // TODO
//        return node.getValue();
//    }
//
//    @Override
//    public Object visit(ASTIntValue node, Object data) {
//        return Integer.toString(node.getValue());
//    }
//
//    public enum VarType {
//        Bool,
//        Number
//    }
//
//    private static class BoolLabel {
//        public String lTrue;
//        public String lFalse;
//
//        public BoolLabel(String lTrue, String lFalse) {
//            this.lTrue = lTrue;
//            this.lFalse = lFalse;
//        }
//    }
//}
package analyzer.visitors;

import analyzer.ast.*;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Objects;
import java.util.Vector;


/**
 * Ce visiteur explore l'AST et génère du code intermédiaire.
 *
 * @author Félix Brunet
 * @author Doriane Olewicki
 * @author Quentin Guidée
 * @version 2023.02.17
 */
public class IntermediateCodeGenFallVisitor implements ParserVisitor {
    private final PrintWriter m_writer;

    public HashMap<String, VarType> SymbolTable = new HashMap<>();

    private int id = 0;
    private int label = 0;

    public IntermediateCodeGenFallVisitor(PrintWriter writer) {
        m_writer = writer;
    }

    private String newID() {
        return "_t" + id++;
    }

    private String newLabel() {
        return "_L" + label++;
    }

    @Override
    public Object visit(SimpleNode node, Object data) {
        return data;
    }

    @Override
    public Object visit(ASTProgram node, Object data) {
        // TODO
        String newLabel = newLabel();
        node.childrenAccept(this, newLabel);
        m_writer.println(newLabel);
        return null;
    }

    @Override
    public Object visit(ASTDeclaration node, Object data) {
        ASTIdentifier id = (ASTIdentifier) node.jjtGetChild(0);
        SymbolTable.put(id.getValue(), node.getValue().equals("bool") ? VarType.Bool : VarType.Number);
        return null;
    }

    @Override
    public Object visit(ASTBlock node, Object data) {
        // TODO
        if(node.jjtGetNumChildren() == 1)
            node.jjtGetChild(0).jjtAccept(this, data);
        else {
            for (int i = 0; i < node.jjtGetNumChildren(); i++) {
                if (i == node.jjtGetNumChildren() - 1)
                    node.jjtGetChild(i).jjtAccept(this, data);
                else {
                    String newlabel = newLabel();
                    node.jjtGetChild(i).jjtAccept(this, newlabel);
                    m_writer.println(newlabel);
                }
            }
        }
        return null;
    }

    @Override
    public Object visit(ASTScriptProgram node, Object data) {
        // TODO
        Vector<String> calledScript = (Vector<String>)node.jjtGetChild(node.jjtGetNumChildren() - 1).jjtAccept(this, data);
        for(int i = 0; i < calledScript.size(); i++ ) {
            for (int j = 0; j < node.jjtGetNumChildren() - 1; j++) {
                if(((ASTScript)node.jjtGetChild(j)).getValue().equals(calledScript.get(i))){
                    node.jjtGetChild(j).jjtAccept(this, data);
                    break;
                }
            }
        }
        return null;
    }

    @Override
    public Object visit(ASTScript node, Object data) {
        node.childrenAccept(this, data);
        return null;
    }

    @Override
    public Object visit(ASTScriptCall node, Object data) {
        // TODO
        Vector<String> identifier = new Vector<String>();
        identifier.add((String)node.jjtGetChild(0).jjtAccept(this, data));
        if(node.jjtGetNumChildren() == 2)
            identifier.addAll((Vector<String>)node.jjtGetChild(1).jjtAccept(this, data));
        return identifier;
    }

    @Override
    public Object visit(ASTStmt node, Object data) {
        node.childrenAccept(this, data);
        return null;
    }

    @Override
    public Object visit(ASTIfStmt node, Object data) {
        // TODO
        String fallLabel = "fall";
        String dataString = (String)data;
        if(node.jjtGetNumChildren() == 2) {
            node.jjtGetChild(0).jjtAccept(this, new BoolLabel(fallLabel, dataString));
            node.jjtGetChild(1).jjtAccept(this, data);
        }else{
            String lfalse = newLabel();
            node.jjtGetChild(0).jjtAccept(this, new BoolLabel(fallLabel, lfalse));
            node.jjtGetChild(1).jjtAccept(this, data);
            m_writer.println("goto " + dataString);
            m_writer.println(lfalse);
            node.jjtGetChild(2).jjtAccept(this, data);
        }
        return null;
    }

    @Override
    public Object visit(ASTWhileStmt node, Object data) {
        // TODO
        String fallLabel = "fall";
        String whileStart = newLabel();
        String dataString = (String)data;
        m_writer.println(whileStart);
        node.jjtGetChild(0).jjtAccept(this, new BoolLabel(fallLabel, dataString));
        node.jjtGetChild(1).jjtAccept(this, whileStart);
        m_writer.println("goto " + whileStart);
        return null;
    }

    @Override
    public Object visit(ASTForStmt node, Object data) {
        // TODO
        String dataString = (String)data;
        String forStart = newLabel();
        String increment = newLabel();
        String ltrue = newLabel();
        node.jjtGetChild(0).jjtAccept(this, new BoolLabel(ltrue, dataString));
        m_writer.println(forStart);
        node.jjtGetChild(1).jjtAccept(this, new BoolLabel(ltrue, dataString));
        m_writer.println(ltrue);
        node.jjtGetChild(3).jjtAccept(this, increment);
        m_writer.println(increment);
        node.jjtGetChild(2).jjtAccept(this, increment);
        m_writer.println("goto " + forStart);
        return null;
    }

    @Override
    public Object visit(ASTAssignStmt node, Object data) {
        String identifier = ((ASTIdentifier) node.jjtGetChild(0)).getValue();
        // TODO
        if(SymbolTable.get(identifier).equals(VarType.Number)) {
            String rightSide = (String) node.jjtGetChild(1).jjtAccept(this, data);
            m_writer.println(identifier + " = " + rightSide);
        }else{
            String fallLabel = "fall";
            String lfalse = newLabel();
            node.jjtGetChild(1).jjtAccept(this, new BoolLabel(fallLabel, lfalse));
            m_writer.println(identifier + " = 1");
            m_writer.println("goto " + data);
            m_writer.println(lfalse);
            m_writer.println(identifier + " = 0");
        }
        return identifier;
    }

    @Override
    public Object visit(ASTExpr node, Object data) {
        return node.jjtGetChild(0).jjtAccept(this, data);
    }

    public Object codeExtAddMul(SimpleNode node, Object data, Vector<String> ops) {
        // À noter qu'il n'est pas nécessaire de boucler sur tous les enfants.
        // La grammaire n'accepte plus que 2 enfants maximum pour certaines opérations, au lieu de plusieurs
        // dans les TPs précédents. Vous pouvez vérifier au cas par cas dans le fichier Langage.jjt.
        // TODO
        if (node.jjtGetNumChildren() == 1)
            return node.jjtGetChild(0).jjtAccept(this, data);

        String firstElem = ops.get(0);
        String newID = newID();
        String firstChild = (String)node.jjtGetChild(0).jjtAccept(this, data);
        String res = newID + " = " + firstChild + " " + firstElem + " " + node.jjtGetChild(1).jjtAccept(this, data);
        m_writer.println(res);
        return newID;
    }

    @Override
    public Object visit(ASTAddExpr node, Object data) {
        return codeExtAddMul(node, data, node.getOps());
    }

    @Override
    public Object visit(ASTMulExpr node, Object data) {
        return codeExtAddMul(node, data, node.getOps());
    }

    @Override
    public Object visit(ASTUnaExpr node, Object data) {
        // TODO
        if(node.getOps().size() == 0)
            return node.jjtGetChild(0).jjtAccept(this, data);

        String child = (String) node.jjtGetChild(0).jjtAccept(this, data);
        String newID = newID();
        m_writer.println(newID + " = " + node.getOps().get(0) + " " + child);

        String oldID = "";
        for(int i = 1; i < node.getOps().size(); i++){
            oldID = newID;
            newID = newID();
            m_writer.println(newID + " = " + node.getOps().get(i) + " " + oldID);
        }
        return newID;
    }

    @Override
    public Object visit(ASTBoolExpr node, Object data) {
        // TODO
        if (node.jjtGetNumChildren() == 1)
            return node.jjtGetChild(0).jjtAccept(this, data);

        String fallLabel = "fall";
        BoolLabel boolData = (BoolLabel)data;
        String op0 =  (String)node.getOps().get(0);
        if (op0.equals("&&")) {
            if (boolData.lFalse != fallLabel) {
                node.jjtGetChild(0).jjtAccept(this, new BoolLabel(fallLabel, boolData.lFalse));
                node.jjtGetChild(1).jjtAccept(this, data);
            } else {
                String newlabel = newLabel();
                node.jjtGetChild(0).jjtAccept(this, new BoolLabel(fallLabel, newlabel));
                node.jjtGetChild(1).jjtAccept(this, data);
                m_writer.println(newlabel);
            }
        }
        else if (op0.equals("||")) {
            if (boolData.lTrue != fallLabel) {
                node.jjtGetChild(0).jjtAccept(this, new BoolLabel(boolData.lTrue, fallLabel));
                node.jjtGetChild(1).jjtAccept(this, data);
            } else {
                String newlabel = newLabel();
                node.jjtGetChild(0).jjtAccept(this, new BoolLabel(newlabel, fallLabel));
                node.jjtGetChild(1).jjtAccept(this, data);
                m_writer.println(newlabel);
            }
        }
//            }
//        }
        return null;
    }

    @Override
    public Object visit(ASTCompExpr node, Object data) {
        // TODO
        if (node.jjtGetNumChildren() == 1)
            return node.jjtGetChild(0).jjtAccept(this, data);

        String leftChild = (String) node.jjtGetChild(0).jjtAccept(this, data);
        String rightChild = (String) node.jjtGetChild(1).jjtAccept(this, data);
        BoolLabel boolData = (BoolLabel)data;
        if (boolData.lTrue == "fall" && boolData.lFalse != "fall") {
            m_writer.println("ifFalse " + leftChild + " " + node.getValue() + " " + rightChild + " goto " + boolData.lFalse);
        }
        else {
            m_writer.println("if " + leftChild + " " + node.getValue() + " " + rightChild + " goto " + boolData.lTrue);
            if (boolData.lFalse != "fall") m_writer.println("goto " + boolData.lFalse);
        }
        return null;
    }

    @Override
    public Object visit(ASTNotExpr node, Object data) {
        // TODO
        Boolean isEven = node.getOps().size() % 2 == 0;
        return node.jjtGetChild(0).jjtAccept(this, isEven ? data : new BoolLabel(((BoolLabel)data).lFalse, ((BoolLabel)data).lTrue));
    }

    @Override
    public Object visit(ASTGenValue node, Object data) {
        // TODO
        return node.jjtGetChild(0).jjtAccept(this, data);
    }

    @Override
    public Object visit(ASTBoolValue node, Object data) {
        // TODO
        String fallLabel = "fall";
        BoolLabel boolData = (BoolLabel) data;
        if (node.getValue() == Boolean.TRUE) {
            if (boolData.lTrue != fallLabel) {
                m_writer.println("goto " + boolData.lTrue);
            }
        }
        else if (boolData.lFalse != fallLabel) {
            m_writer.println("goto " + boolData.lFalse);
        }
        return null;
    }

    @Override
    public Object visit(ASTIdentifier node, Object data) {
        // TODO
        String identifier = node.getValue();
        String fallLabel = "fall";

        if (SymbolTable.get(identifier) == VarType.Bool) {
            BoolLabel boolData = (BoolLabel)data;
            if (boolData.lTrue != fallLabel && boolData.lFalse != fallLabel) {
                m_writer.println("if " + identifier + " == 1 goto " + boolData.lTrue);
                m_writer.println("goto " + boolData.lFalse);
            } else m_writer.println((boolData.lTrue == fallLabel ? "ifFalse " + identifier + " == 1 goto " + boolData.lFalse : "if " + identifier + " == 1 goto " + boolData.lTrue));
        }
        return identifier;
    }

    @Override
    public Object visit(ASTIntValue node, Object data) {
        return Integer.toString(node.getValue());
    }

    public enum VarType {
        Bool,
        Number
    }

    private static class BoolLabel {
        public String lTrue;
        public String lFalse;

        public BoolLabel(String lTrue, String lFalse) {
            this.lTrue = lTrue;
            this.lFalse = lFalse;
        }
    }
}

